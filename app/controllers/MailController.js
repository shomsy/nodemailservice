var nodemailer = require('nodemailer');
var credentials = require('../../config/mail');

var transporter = nodemailer.createTransport({
    pool: true,
    maxConnections: 5,
    service : credentials.test.service,
    host: credentials.test.service,
    auth: {
        user: credentials.test.email,
        pass: credentials.test.password
    }
});

module.exports = {

    /**
     * Send Mail function.
     */
    send: function (req, res, to, subject, template) {

        var mailOptions = {
            from: credentials.test.email,
            to: to,
            subject: subject,
            html: template
        };
        
      return transporter.sendMail(mailOptions).then(function(info){
            return info;
        });
    },
}
