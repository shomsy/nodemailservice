var kue = require('kue');
var queue = kue.createQueue();
var email = require('./MailController');
kue.app.listen(9999);

module.exports = {

    // sending limit per time period.
    emails_limit: 5,

    // Promise whole process.
    wholeQueue: function (req, res, done) {
        res.end();
        var that = this;
        return this.countQueded().then(function (queded) {
                if (queded > 0) {
                    return that.processJob();
                } else {
                    throw 'No_Jobs_At_The_Moment';
                }
            }).then(function () {
                var A = that.countActive().then(function (count) {
                    if (count > 0) {
                        return that.stuckedActiveToQueded();
                    }
                })

                var F = that.countFailedJobs().then(function (count) {
                    if (count > 0) {
                        return that.failedJobToQueded();
                    }
                })

                var C = that.countCompleteStuckedJobs().then(function (count) {
                    if (count > 0) {
                        return that.removeCompletedJobs();
                    }
                })

                return Promise.all([A, F, C]);

            })
            .catch(function (err) {
                if (err === 'No_Jobs_At_The_Moment') {
                    return;
                } else {
                    throw err;
                }

            });
    },

    // ToDo Make JSON object to have list of email addresses, and priorities for each of them.
    createJob: function (req, res, done) {
        var that = this;
        var json = req.body;
        for (var i = 0; i < json.length; i++) {
            queue.create('sendmail', {
                    email: json[i].email,
                    subject: json[i].subject,
                    user_name: json[i].user_name,
                    first_name: json[i].first_name,
                    last_name: json[i].last_name,
                    template:json[i].template,
                    model: json[i].model
                }).priority(json[i].priority).attempts(5).removeOnComplete(true)
                .save(function (err) {
                    if (err) {
                        process.exit(0);
                        res.end("error for job with id " + job.id);
                    } else {
                        console.log('Succesful added job, check panel at http://localhost:9999');
                    }
                });
        }

        res.end('Succesful added job, check panel at http://localhost:9999');
    },

    // Do the job.
    processJob: function (req, res, done) {
        var that = this;
        var limit = this.emails_limit;
        var sent = 0;
        return new Promise(function (resolve, reject) {  
                while(sent < limit) {
                    queue.process('sendmail', function (job, done) {
                        email.send(req, res, job.data.email, job.data.subject, that._parseBindings(job.data.template, job.data.model))
                            .then(function (response) {
                                return {
                                    accepted: response.accepted,
                                    rejected: response.rejected
                                }
                            })
                            .then(function (response) {
                                // what to do when is rejected?
                                resolve();
                            })
                            .catch(function (err) {
                                console.log(err);
                            }); 
                        console.log('Job number ' + job.id + ' and email' + job.data.email + 'and whith ' + job.data.priority + ' priority has been proccessed.', sent); 
                        done();
                    });
                    sent++;    
                }
            
        });
    },

    // number of Inactive jobs aka. Queded.
    countQueded: function (req, res, done) {
        return new Promise(function (resolve, reject) {
            queue.inactive(function (err, ids) {
                if (err) {
                    reject(err);
                } else {
                    resolve(ids.length);
                }
            });
        })
    },

    // number of Active jobs.
    countActive: function (req, res, done) {
        return new Promise(function (resolve, reject) {
            queue.active(function (err, ids) {
                if (err) {
                    reject(err);
                } else {
                    resolve(ids.length);
                }
            });
        })
    },

    // number of failed jobs.
    countFailedJobs: function () {
        return new Promise(function (resolve, reject) {
            queue.failedCount(function (err, total) {
                resolve(total);
            });
        });
    },

    //Turn failed back to Queded.
    failedJobToQueded: function () {
        return new Promise(function (resolve, reject) {
            queue.failed(function (err, ids) {
                ids.forEach(function (id) {
                    kue.Job.get(id, function (err, job) {
                        resolve(job.inactive());
                    });
                });
            });
        });
    },

    // Turn stucked active into Queded.
    stuckedActiveToQueded: function (req, res) {
        return new Promise(function (resolve, reject) {
            queue.active(function (err, ids) {
                ids.forEach(function (id) {
                    kue.Job.get(id, function (err, job) {
                        resolve(job.inactive());
                    });
                });
            });
        });
    },

    // number of completed jobs, if stucked (are not autocompleted)
    countCompleteStuckedJobs: function () {
        return new Promise(function (resolve, reject) {
            queue.completeCount(function (err, total) {
                resolve(total);
            });
        });
    },

    // manual finished job removal, if needed.
    removeCompletedJobs: function (req, res, done) {
        return new Promise(function (resolve, reject) {
            kue.Job.rangeByState('complete', 0, -1, 'asc', function (err, selectedJobs) {
                selectedJobs.forEach(function (job) {
                    resolve(job.remove());
                });
            })
        })
    },

    _modelValueFromPath: function (model, path) {
        var pathArray = path.split('.'),
            value = model;
            console.log(pathArray,model);
        while (pathArray.length && value != null) {
            path = pathArray.shift();
            value = value[path];
        }
        return value;
    },

    _parseBindings: function (html, model) {
        var that = this;
        return html.replace(/\[\[([\w|.]+)]]/g, function (full, match) {
            return that._modelValueFromPath(model, match);
        });
    },

    _parseRepeaters: function(input,model){
        return input.replace(/\{\{repeat\s(\w+)}}(.+?)\{\{\/repeat}}/g, function (full,arrayKey, match) {
            var output = "";
            if(!model[arrayKey]){
                return output;
            }
            for (var i = 0; i < model[arrayKey].length; i++) {
                output += match.replace(/\{\{index}}/g, i);
            }
            return output;
        });
    },
    _modelValueFromPath: function (model,path) {
       var pathArray = path.split('.'),
       value = model;
       while (pathArray.length && value != null) {
         path = pathArray.shift();
         value = value[path];
     }
     return value;
 },
     _parseBindings: function (html,model) {
        var that = this;  
        return this._parseRepeaters(html,model).replace(/\[\[([\w|.]+)]]/g, function (full, match) {
         return that._modelValueFromPath(model,match);
     });
}


};