var express = require('express');
var router = express.Router();
var Queue = require('../app/controllers/QueueController');

// QueueController
router.post('/create', Queue.createJob.bind(Queue));
router.get('/process', Queue.processJob.bind(Queue));
router.get('/queded', Queue.countQueded.bind(Queue));
router.get('/active', Queue.countActive.bind(Queue));
router.get('/removeJobs', Queue.removeCompletedJobs.bind(Queue));
router.get('/stuckedActiveToQueded', Queue.stuckedActiveToQueded.bind(Queue));
router.post('/fire', Queue.wholeQueue.bind(Queue));

module.exports = router;