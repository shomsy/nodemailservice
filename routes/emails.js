var express = require('express');
var route = express.Router();
var mail = require('../app/controllers/MailController');

route.post('/send', mail.send);
module.exports = route;